import apartments from "../data";

export default defineEventHandler((event) => {
  const { id } = getQuery(event);
  return apartments.find((apartment) => apartment.id == id);
});
