// Mock data
export default [
  {
    floor: 1,
    door_number: 5,
    id: 0,
    address: "366 Reed Street, Leland, Florida, 2353",
  },
  {
    floor: 3,
    door_number: 9,
    id: 1,
    address: "370 Arlington Place, Manila, Maine, 5741",
  },
  {
    floor: 1,
    door_number: 9,
    id: 2,
    address: "713 Clark Street, Axis, South Dakota, 7260",
  },
  {
    floor: 5,
    door_number: 8,
    id: 3,
    address: "442 Riverdale Avenue, Bethany, New Jersey, 5508",
  },
  {
    floor: 1,
    door_number: 7,
    id: 4,
    address: "949 Ralph Avenue, Orick, West Virginia, 4503",
  },
  {
    floor: 5,
    door_number: 10,
    id: 5,
    address: "905 Bevy Court, Osmond, Tennessee, 5717",
  },
  {
    floor: 5,
    door_number: 5,
    id: 6,
    address: "391 Wilson Avenue, Dorneyville, New Hampshire, 9506",
  },
  {
    floor: 3,
    door_number: 7,
    id: 7,
    address: "520 Burnett Street, Brownsville, Virginia, 7423",
  },
  {
    floor: 2,
    door_number: 1,
    id: 8,
    address: "879 Mill Street, Gerber, California, 7359",
  },
  {
    floor: 5,
    door_number: 9,
    id: 9,
    address: "335 Merit Court, Churchill, Oklahoma, 2289",
  },
];
